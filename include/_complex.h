#pragma once
#include <math.h>

struct Complex {
    float re,im;
};

Complex make_complex(float r, float i);

Complex make_complex_exp(float r, float theta_deg);

Complex operator+(Complex a, Complex b);

Complex operator-(Complex a, Complex b);

Complex operator*(Complex a, Complex b);

Complex operator*(float lambda, Complex a);

Complex scale(Complex p, float cx, float cy, float lambda);

Complex rotate(Complex p, float cx, float cy, float theta_deg);

float module(Complex a);
