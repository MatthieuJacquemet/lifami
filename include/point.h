#pragma once
#include <math.h>

struct Point {
    float x,y;
};

Point make_point(float r, float i);

Point make_point_exp(float r, float theta_deg);

Point operator+(Point a, Point b);

Point operator-(Point a, Point b);

Point operator*(Point a, Point b);

Point operator*(float lambda, Point a);

Point scale(Point p, float cx, float cy, float lambda);

Point rotate(Point p, float cx, float cy, float theta_deg);

float module(Point a);
