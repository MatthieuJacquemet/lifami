#include <math.h>

struct Vec2 {
    float x, y;
};

Vec2 operator+(Vec2 a, Vec2 b);
Vec2 operator-(Vec2 a, Vec2 b);
Vec2 operator*(float lambda, Vec2 b);
Vec2 operator/(Vec2 b, float lambda);
float length(Vec2 v);
float distance(Vec2 p1, Vec2 p2);
Vec2 normalize(Vec2 v);
float dot(Vec2 v0, Vec2 v1);
Vec2 make_vec2(float x, float y);
Vec2 make_vec2_exp(float r, float theta_deg);
Vec2 scale_vec2(Vec2 p, Vec2 c, float lambda);
Vec2 rotate_vec2(Vec2 p, Vec2 c, float theta);
Vec2 lerp_vec2(Vec2 v0, Vec2 v1, float t);
Vec2 operator*(Vec2 a, Vec2 b);