struct Color {
    unsigned char r,g,b;
};

Color operator+(Color a, Color b) {
    return {a.r+b.r, a.g+b.g, a.b+b.b};
}

Color operator*(float a, Color b) {
    return {b.r*a, b.g*a, b.b*a};
}

Color color_lerp(Color a, Color b, float t) {
    return (1-t)*a + t*b;
}