#include "vec2.h"

Vec2 make_vec2(float x, float y) {
    return {x, y};
}

Vec2 operator+(Vec2 a, Vec2 b) {
    return {a.x+b.x, a.y+b.y};
}

Vec2 operator-(Vec2 a, Vec2 b) {
    return {a.x-b.x, a.y-b.y};
}

Vec2 operator*(float lambda, Vec2 b) {
    return {b.x*lambda, b.y*lambda};
}
Vec2 operator*(Vec2 a, Vec2 b) {
    return make_vec2(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}

Vec2 operator/(Vec2 b, float lambda) {
    return {b.x/lambda, b.y/lambda};
}

float length(Vec2 v) {
    return sqrt(pow(v.x,2)+pow(v.y,2));
}

float distance(Vec2 p1, Vec2 p2) {
    return length(p2-p1);
}

Vec2 normalize(Vec2 v) {
    if (length(v) > 0)
        return v/length(v);
    return {0,0};
}

float dot(Vec2 v0, Vec2 v1) {
    return v0.x*v1.x + v0.y*v1.y;
}

Vec2 make_vec2_exp(float r, float theta) {
    // float rad = theta_deg * M_PI / 180;
    return {r*cos(theta), r*sin(theta)};
}

Vec2 scale_vec2(Vec2 p, Vec2 c, float lambda){
    // Vec2 t = make_vec2(cx,cy);
    return (lambda *(p-c)) + c ;
}

Vec2 rotate_vec2(Vec2 p, Vec2 c, float theta_deg){
    // Vec2 t = make_vec2(cx, cy);
    Vec2 angle = make_vec2_exp(1,theta_deg);
    return ((p-c)*angle) + c ;
}

Vec2 lerp_vec2(Vec2 v0, Vec2 v1, float t) {
    return (1-t)*v0 + t*v1;
}