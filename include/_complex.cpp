#include <cmath>
#include "_complex.h"

using namespace std;

Complex make_complex(float r, float i) {
    return {r, i};
}

Complex make_complex_exp(float r, float theta_deg) {
    float rad = theta_deg * M_PI / 180;
    return {r*cos(rad), r*sin(rad)};
}

Complex operator+(Complex a, Complex b) {
    return {a.re + b.re, a.im + b.im};
}

Complex operator-(Complex a, Complex b) {
    return {a.re - b.re, a.im - b.im};
}

Complex operator*(Complex a, Complex b) {
    return make_complex(a.re*b.re - a.im*b.im, a.re*b.im + a.im*b.re);
}

Complex operator*(float lambda, Complex a) {
    return {lambda*a.re , lambda*a.im};
}

Complex scale(Complex p, float cx, float cy, float lambda){
    Complex t = make_complex(cx,cy);
    return (lambda *(p-t)) + t ;
}

Complex rotate(Complex p, float cx, float cy, float theta_deg){
    Complex t = make_complex(cx, cy);
    Complex angle = make_complex_exp(1,theta_deg);
    return ((p-t)*angle) + t ;
}

float module(Complex a) {
    return sqrt(a.re*a.re + a.im*a.im);
}