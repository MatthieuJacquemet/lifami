#include <cmath>
#include "point.h"

using namespace std;

Point make_point(float x, float y) {
    return {x, y};
}

Point make_point_exp(float r, float theta_deg) {
    float rad = theta_deg * M_PI / 180;
    return {r*cos(rad), r*sin(rad)};
}

Point operator+(Point a, Point b) {
    return {a.x + b.x, a.y + b.y};
}

Point operator-(Point a, Point b) {
    return {a.x - b.x, a.y - b.y};
}

Point operator*(Point a, Point b) {
    return make_point(a.x*b.x - a.y*b.y, a.x*b.y + a.y*b.x);
}

Point operator*(float lambda, Point a) {
    return {lambda*a.x , lambda*a.y};
}

Point scale(Point p, float cx, float cy, float lambda){
    Point t = make_point(cx,cy);
    return (lambda *(p-t)) + t ;
}

Point rotate(Point p, float cx, float cy, float theta_deg){
    Point t = make_point(cx, cy);
    Point angle = make_point_exp(1,theta_deg);
    return ((p-t)*angle) + t ;
}

float module(Point a) {
    return sqrt(a.x*a.x + a.y*a.y);
}