#!/usr/bin/env bash

clear

if [ ! -z "$1" -a ! -z "$2" ]; then
    if egrep -q "^.+/src/.+/.+\.cpp$"<<<"$1" ; then

        # Build the target
        TARGET="$(realpath --relative-to="$4" "${1%.*}" | sed "s/\//_/")" && \
        eval "$(sed -r "s/(.*--target )[^ ]+/\1$TARGET/"<<<"$2")" && \

        # Run the target
        (cd "$3" && ./"$TARGET")
        
    else
        echo "Please select a source file in src"
    fi
fi