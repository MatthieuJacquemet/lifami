

#include <iostream>
#include <Grapic.h>
#include <cmath>
#include "vec2.h"
#include "_color.h"
#include <unistd.h>

using namespace std;
using namespace grapic;

const int DIMW = 700;
const int MAX = 512;
const int WHEEL_SEG = 20;
const float K = 1;
const float RHO = 1.225; // air 
const float BALL_DRAG_COEFF = 0.5;
const float FRICTION = 0.8f;
const float INITIAL_VEL = 60;

struct Particle {
    float m;
    float r;
    Vec2 p, v, f;
};

struct Spring {
    float k, l, p;
    int i1, i2;
};

struct World {
    Particle p[MAX];
    Spring s[MAX];
    int np, ns;
    float dt;
    int menu_select;
};

Vec2 make_vec(float x, float y) {
    return {x, y};
}

void init_world(World& w) {
    w.np = 0;
    w.ns = 0;
    w.menu_select = 0;
}

int add_particle(World& w, float x, float y, float m) {

    w.p[w.np].f.x   = 0;
    w.p[w.np].f.y   = 0;

    w.p[w.np].p.x   = x;
    w.p[w.np].p.y   = y;

    w.p[w.np].v.x   = 0;
    w.p[w.np].v.y   = 0;

    // w.p[w.np].r     = m*5;
    w.p[w.np].r     = pow((3*m)/(M_PI*4), 1.0/3);
    w.p[w.np].m     = m;

    w.np++;
    return w.np-1;
}

void draw_particle(Particle &p) {
    // color(color_lerp({255,0,0},{0,0,255},));
    color(rand()%256,rand()%256,rand()%256);
    circleFill(p.p.x, p.p.y, p.r*10);
}

void add_force(Particle &p, Vec2 force) {
    p.f = p.f + force;
}

void collision(Particle& p) {

    if (p.p.y < p.r) {
        p.p.y = p.r-(p.p.y-p.r);
        p.v.y = -p.v.y;
        p.v = FRICTION * p.v;
    }
    if (p.p.y > DIMW-p.r) {
        p.p.y = DIMW-p.r-(p.p.y-(DIMW-p.r));
        p.v.y = -p.v.y;
        p.v = FRICTION * p.v;
    }
    if (p.p.x < p.r) {
        p.p.x = p.r-(p.p.x-p.r);
        p.v.x = -p.v.x;
        p.v = FRICTION * p.v;
    }
    if (p.p.x > DIMW-p.r) {
        p.p.x = DIMW-p.r-(p.p.x-(DIMW-p.r));
        p.v.x = -p.v.x;
        p.v = FRICTION * p.v;
    }
}

int add_spring(World& w, int i1, int i2) {

    w.s[w.ns].i1    = i1;
    w.s[w.ns].i2    = i2;
    w.s[w.ns].k     = K;
    w.s[w.ns].l     = distance(w.p[i1].p,w.p[i2].p);
    w.s[w.ns].p     = 0;
    w.ns++;
    return w.ns-1;
}

void update_particle(World &w, float dt) {

    for (int i=0; i<w.np; i++) {
        if (w.p[i].m > 0) {
            float drag = pow(length(w.p[i].v),2)*M_PI*pow(w.p[i].r,2)*RHO*BALL_DRAG_COEFF*0.5;
            w.p[i].f = w.p[i].f - drag*normalize(w.p[i].f);
            w.p[i].v = w.p[i].v + dt*(w.p[i].f/w.p[i].m);
            w.p[i].p = w.p[i].p + dt*w.p[i].v;
            w.p[i].f = make_vec(0,0);
            collision(w.p[i]);
        }
    }
}

void init_line(World& w) {
    if (w.np < MAX-1){
        float x = frand(DIMW/2 - 100, DIMW/2 + 100);
        float y = frand(DIMW/2 - 100, DIMW/2 + 100);
        float weight = frand(1, 4);  
        int i1 = add_particle(w, x, y, weight);
        w.p[i1].v.x   = frand(-INITIAL_VEL,INITIAL_VEL);
        w.p[i1].v.y   = frand(-INITIAL_VEL,INITIAL_VEL);

        x = frand(DIMW/2 - 20, DIMW/2 + 20);
        y = frand(DIMW/2 - 20, DIMW/2 + 20);
        weight = frand(1, 4);
        int i2 = add_particle(w, x, y, weight);
        w.p[i2].v.x   = frand(-INITIAL_VEL,INITIAL_VEL);
        w.p[i2].v.y   = frand(-INITIAL_VEL,INITIAL_VEL);
        add_spring(w, i1, i2);
    }
}

void compute_spring_force(World& w) {
    for (int i=0; i<w.ns; i++) {
        Spring& s = w.s[i];
        float len = distance(w.p[s.i1].p, w.p[s.i2].p) - s.l;
        Vec2 direction = normalize(w.p[s.i1].p-w.p[s.i2].p);
        Vec2 f = len*s.k*direction;
        add_force(w.p[s.i1], -1.0*f);
        add_force(w.p[s.i2], f);
    }
}

void compute_gravity_force(World& w) {
    for (int i=0; i<w.np; i++)
        add_force(w.p[i], {0,w.p[i].m*-9.81});
}

void update_world(World& w, float dt) {

    if (isKeyPressed(SDLK_SPACE)){
        init_line(w);
        // add_particle(w,DIMW/2, DIMW/2, 1);
    }
    compute_gravity_force(w);
    compute_spring_force(w);
    update_particle(w, dt);
}

void draw_world(World w) {

    color(255,255,255); 
    for (int i=0; i<w.ns; i++) {
        Vec2 p1 = w.p[w.s[i].i1].p;
        Vec2 p2 = w.p[w.s[i].i2].p;
        line(p1.x,p1.y,p2.x,p2.y);
    }
    for (int i=0; i<w.np; i++)
        draw_particle(w.p[i]);
}



void init_wheel(World& w) {
    init_world(w);
    float sec_angle = M_PI*2/WHEEL_SEG;
    // int center_id = add_particle(w, DIMW/2, DIMW/2, 1);

    for (int i=0; i<WHEEL_SEG; i++) {
        float x = cos(sec_angle*i)*100;
        float y = sin(sec_angle*i)*100;
        int id = add_particle(w, DIMW/2+x, DIMW/2+y, 1);
        for (int j=id; j<WHEEL_SEG; j++) {
            if (j!=id) {
                add_spring(w, id, j);
                cout << id << " " << j << endl;
            }
        }
        // add_spring(w, id, (id%WHEEL_SEG));
        // add_spring(w, id, center_id);
        // add_spring(w, id, id+1);
    }


}

void init_square(World& w) {
    init_world(w);
    int p0 = add_particle(w,DIMW/2-50, DIMW/2-50, 1);
    int p1 = add_particle(w,DIMW/2-50, DIMW/2+50, 1);
    int p2 = add_particle(w,DIMW/2+50, DIMW/2-50, 1);
    int p3 = add_particle(w,DIMW/2+50, DIMW/2+50, 1);

    add_spring(w, p0, p1);
    add_spring(w, p0, p3);
    add_spring(w, p0, p2);

    add_spring(w, p1, p3);
    add_spring(w, p1, p2);
    add_spring(w, p2, p3);
}

void update_menu(Menu& menu, World& w) {
    switch(menu_select(menu)) {
        case 0:
            init_world(w);
            init_line(w);
            menu_setSelect(menu, 3);
            break;
        case 1:
            init_square(w);
            menu_setSelect(menu, 3);
            break;
        case 2:
            init_wheel(w);
            menu_setSelect(menu, 3);
            break;
        default: break;
    }
    // init_line(w);
}

int main(int, char **)
{
    bool stop = false;
    winInit("particle", DIMW, DIMW);
    backgroundColor(0, 0, 0);
    // Particle p;
    // partInit(p);
    Menu menu;
    menu_add(menu, "line");
    menu_add(menu, "square");
    menu_add(menu, "wheel");
    menu_add(menu, "run");
    menu_setSelect(menu, 0);

    World world;
    init_world(world);

    float start = 0, dt;
    while (!stop) {
        dt = (elapsedTime() - start);
        start = elapsedTime();
        winClear();
        update_menu(menu, world);
        update_world(world, dt*10);
        draw_world(world);
        menu_draw(menu);
        // update_part(p);
        // draw_part(p);
        stop = winDisplay();
        // usleep(100000);
    }

    winQuit();

    return 0;
}
