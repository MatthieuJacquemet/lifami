#include <Grapic.h>
#include <cmath>
#include <iostream>
#include "_complex.h"

using namespace grapic;
using namespace std;
const int DIMW = 500;

struct SolarSystem
{
    Complex Sun, Mercure, Moon, Earth ;
    Image imSun, imEarth, imMoon, imMercure , imFond;
};

void initSS (struct SolarSystem &sys)
{
    sys.Sun = make_complex(DIMW/2, DIMW/2);
    sys.Earth = sys.Sun + make_complex(120,0);
    sys.Mercure = sys.Sun + make_complex(60,0);
    sys.Moon = sys.Earth + make_complex(25,0);

    sys.imEarth = image("data/earth.jpg");
    sys.imMoon = image("data/moon.jpg");
    sys.imSun = image("data/sun.jpg");
    sys.imMercure = image("data/mercure.jpg");
    sys.imFond = image("data/fond.jpg");
}

void draw (struct SolarSystem sys)
{
    /*color (255,255,0); /// jaune pour le soleil
    circleFill(sys.Sun.re, sys.Sun.im, 10);
    color (255,0,0); /// rouge pour mercure
    circleFill(sys.Mercure.re, sys.Mercure.im, 4);
    color (0,0,255); /// bleu pour la terre
    circleFill(sys.Earth.re, sys.Earth.im, 5);
    color (120,120,120); /// gris pour la lune
    circleFill(sys.Moon.re, sys.Moon.im, 2);*/
    image_draw(sys.imFond, 0,0,DIMW,DIMW);
    image_draw(sys.imSun,sys.Sun.re, sys.Sun.im,30,30);
    image_draw(sys.imMercure,sys.Mercure.re, sys.Mercure.im,15,15);
    image_draw(sys.imEarth,sys.Earth.re, sys.Earth.im,20,20);
    image_draw(sys.imMoon,sys.Moon.re, sys.Moon.im,10,10);


}

void update (struct SolarSystem &sys)
{
    sys.Mercure = rotate(sys.Mercure, sys.Sun.re, sys.Sun.im, 0.1f);
    Complex lune = sys.Moon - sys.Earth ;
    sys.Earth = rotate(sys.Earth, sys.Sun.re, sys.Sun.im, 0.03f);
    sys.Moon = sys.Earth + lune ;
    sys.Moon = rotate(sys.Moon, sys.Earth.re, sys.Earth.im, 0.3f);
}

int main(int argc, char** argv)
{
    bool stop=false;
      winInit("Complexes", DIMW, DIMW);// Créer une fenêtre (taille 500x500)
    backgroundColor( 0, 0, 0 );
    SolarSystem SS;
    winClear();
    initSS(SS);
    while (!stop)
    {
        winClear();
        update(SS);
        draw(SS);
        stop = winDisplay();
    }
                                        // Affiche réellement à l’écran
    pressSpace();
                        // Attend l’appui sur « espace » pour fermer
    winQuit();
                               // Ferme la fenêtre et quitte
    return 0; 
}