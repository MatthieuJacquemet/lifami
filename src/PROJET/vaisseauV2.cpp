#include <iostream>
#include <Grapic.h>
#include <ctime>
#include <iomanip>

#include "vec2.h"

const int DIMW_X = 500;
const int DIMW_Y = 500;
const int MAX_IMG = 32;

using namespace std;
using namespace grapic;

struct Surface {
    Vec2 pos[4];
    Vec2 uv[4];
    Vec2 center;
    float width; // largeur BA
    float height; // hauteur DA

    int z_index;
    int texture_id;
};
void translate_surface(Surface& s,Vec2 position){
    for(int i=0;i<4;i++){
        s.pos[i]=s.pos[i]+position;
    }
    s.center=s.center+position;
}

void rotate_surface(Surface& s,float theta){
    for(int i=0;i<4;i++){
        s.pos[i]=rotate_vec2(s.pos[i],s.center,theta);
    }
}

void scale_surface(Surface& s,float scale){
    for(int i=0;i<4;i++){
        s.pos[i]=scale_vec2(s.pos[i],s.center,scale);
    }
    s.height = scale*s.height;
    s.width  = scale*s.width;
}

struct TextureContainer {
    Image textures[MAX_IMG];
    int n;
};

void init_textures(TextureContainer& tc) {
    tc.n = 0;
}

void add_texture(const char* path, TextureContainer& tc){
     if(tc.n<MAX_IMG){
         tc.textures[tc.n] = image(path);
         tc.n++;
     }
     else
         cout<<"Plus de place pour les textures.."<<endl;
}

void b_box(Surface& s, float& min_x, float& max_x,
    float& min_y, float& max_y)
{
    max_x = 0;
    max_y = 0;
    min_x = DIMW_X;
    min_y = DIMW_Y;

    for (int i = 0; i < 4; i++) {
        if (s.pos[i].x < min_x)
            min_x = s.pos[i].x;
        if (s.pos[i].x > max_x)
            max_x = s.pos[i].x;
        if (s.pos[i].y < min_y)
            min_y = s.pos[i].y;
        if (s.pos[i].y > max_y)
            max_y = s.pos[i].y;
    }
}

void compute_coords(Surface& s) {

    s.uv[0] = make_vec2(0,0);
    s.uv[1] = make_vec2(0,s.height);
    s.uv[2] = make_vec2(s.width,s.height);
    s.uv[3] = make_vec2(s.width,0);

    Vec2 offset = make_vec2(s.width/2, s.height/2);
    for (int i=0; i<4; i++){
        s.pos[i] = s.uv[i] + s.center - offset;
    }
}

void set_surface_texture(Surface& s, int id, TextureContainer& tc){

    if(id < tc.n){
        const SDL_Surface* surface = tc.textures[id].surface();
        s.height = surface->h;
        s.width  = surface->w;
        s.texture_id = id;
        compute_coords(s);
    }
}


Surface make_surface(int texture_id, TextureContainer& tc) {

    Surface s;

    s.texture_id = -1;
    s.width  = 1;
    s.height = 1;

    set_surface_texture(s, texture_id, tc);

    s.center = make_vec2(s.width/2, s.height/2);
    s.z_index = 0;

    compute_coords(s);

    return s;
}

void sample_texture(Vec2 uv, Image& tex,
    unsigned char& r, unsigned char& g, unsigned char& b, unsigned char&a)
{
    r = tex.get(uv.x, uv.y, 0);
    g = tex.get(uv.x, uv.y, 1);
    b = tex.get(uv.x, uv.y, 2);
    a = tex.get(uv.x, uv.y, 3);
}

void draw_rectangle(Surface& s, TextureContainer& tc) {

    // get the bounding box of the rectangle
    float max_x, min_x, max_y, min_y;
    b_box(s, min_x, max_x, min_y, max_y);

    // precompute denominators
    float height_d = pow(s.height,2);
    float width_d  = pow(s.width,2);

    // two edges of the rectangle
    Vec2 v0 = s.pos[1]-s.pos[0];
    Vec2 v1 = s.pos[3]-s.pos[0];
    float total = 0;
    
    for (int x = min_x; x < max_x; x++) {
        for (int y = min_y; y < max_y; y++) {
            
            Vec2 p  = make_vec2(x,y)-s.pos[0];

            // project fragment onto rectangle space
            float t_height = dot(p,v0)/height_d;
            float t_width  = dot(p,v1)/width_d;

            if( 0<=t_height && t_height <= 1
                && 0<=t_width && t_width <= 1)
            {
                // float st = elapsedTime();

                // bilinear interpolation
                Vec2 h_l= lerp_vec2(s.uv[0],s.uv[1],t_height);
                Vec2 h_r= lerp_vec2(s.uv[3],s.uv[2],t_height);
                Vec2 uv = lerp_vec2(h_l,h_r,t_width);

                // other interpolation methode
                // float t_2 = t_width*t_height;
                // float t_1 = (1.0-t_width)*t_height;
                // float t_3 = t_width*(1.0-t_height);
                // float t_0 = 1.0 - t_1 - t_2 - t_3;
                // Vec2 uv = t_0*s.uv[0] + t_1*s.uv[1] + t_2*s.uv[2] + t_3*s.uv[3];

                // total += elapsedTime() - st;

                // add half-pixel offset for sampling texel center
                uv = uv + make_vec2(0.5,0.5);

                // sample texel and apply it to final image
                unsigned char r, g, b, a;
                sample_texture(uv, tc.textures[s.texture_id], r, g, b, a);
                put_pixel(x, y, r, g, b, a);

            }
            // else put_pixel(x, y, 255,0,0); // debug discarded fragments
        }
    }
    // cout << total << endl;
}

int main(int, char**){
    bool stop = false;
    winInit("Game", DIMW_X, DIMW_Y);
    backgroundColor(0,0,0);

    TextureContainer tc;
    init_textures(tc);
    // add_texture("data/pooh.png",tc);
    add_texture("data/bricks.jpg",tc);
    
    Surface s;
    s = make_surface(0,tc);
    translate_surface(s,{201,201});
    set_surface_texture(s,1,tc);
    // scale_surface(s,1);

    float start_time = 0, delta_time, shift = 0;
    int count = 0, fps = 0;

    while (!stop) {

        count++;
        if (start_time-shift >= 1){
            shift = start_time;
            fps = count;
            count = 0;
        }
        
        delta_time = elapsedTime() - start_time;
        start_time = elapsedTime();
        // cout << delta_time << " ";
        
        rotate_surface(s,delta_time);
        draw_rectangle(s,tc);

        color(0,255,0);
        print(10, DIMW_Y-30, ("FPS : "+to_string(fps)).c_str());

        stop = winDisplay();
        winClear();

    }
    
    winQuit();
    return 0;
}