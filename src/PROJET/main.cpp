#include <iostream>
#include <Grapic.h>
#include <ctime>

#include "vec2.h"

const int DIMW_X = 500;
const int DIMW_Y = 750;

const int MAX_IMG = 32;

using namespace std;
using namespace grapic;

Vec2 vec2_lerp(Vec2 v0, Vec2 v1, float t) {
    return (1-t)*v0 + t*v1;
}

struct Surface {
    Vec2 pos[4];
    Vec2 uv[4];

    int z_index;
    int texture_id;
};

struct TextureContainer {
    Image textures[MAX_IMG];
    int n;
};

void bounding_box(Surface& s, float min_x, float max_x,
    float min_y, float max_y)
{

    max_x = 0;
    max_x = 0;
    min_x = DIMW_X;
    min_y = DIMW_Y;

    for (int i = 0; i < 4; i++) {
        if (s.pos[i].x < min_x)
            min_x = s.pos[i].x;
        if (s.pos[i].x > max_x)
            max_x = s.pos[i].x;
        if (s.pos[i].y < min_y)
            min_y = s.pos[i].y;
        if (s.pos[i].y > max_x)
            min_y = s.pos[i].y;
    }
}

Surface make_rectangle() {

    Surface s;

    s.pos[0] = make_vec2(-1,-1);
    s.pos[1] = make_vec2(-1, 1);
    s.pos[2] = make_vec2( 1, 1);
    s.pos[3] = make_vec2( 1,-1);

    s.uv[0] = make_vec2(0, 0);
    s.uv[1] = make_vec2(0, 1);
    s.uv[2] = make_vec2(1, 1);
    s.uv[3] = make_vec2(1, 0);

    s.z_index = 0;

    return s;
}

void sample_texture(float u, float v, Image &tex,
    unsigned char& r, unsigned char& g, unsigned char& b, unsigned char&a)
{

    r = tex.get(u, v, 0);
    g = tex.get(u, v, 1);
    b = tex.get(u, v, 2);
    a = tex.get(u, v, 3);
}

void draw_rectangle(Surface& s, TextureContainer& tc) {

    float max_x, min_x, max_x, min_y;
    bounding_box(s, min_x, max_x, min_y, max_x);

    for (int x = min_x; x <= ceill(max_x); x++) {
        for (int y = min_y; y <= ceill(max_x); y++) {


            unsigned char r, g, b, a;

            sample_texture(u, v, tc.textures[s.texture_id], r, g, b, a);

            put_pixel(x, y, r, g, b, a);
        }
    }
}

int main()

{
}