#include "Grapic.h"

#include <math.h>
#include <iostream>
#include <algorithm>

#include "_complex.h"

using namespace std;
using namespace grapic;

const int DIMW = 800;
int max_iter = 30;
Complex C = {-0.0986,-0.65186};
float scale_factor = 1.0;
float limite = 4.0;

// void couleur_julia(int n, unsigned char& r, unsigned char& g, unsigned char& b)
// {
//     float c = (float(n))/max_iter;
//     r = c*255;
//     g = 128 + c*128;
//     b = 64  + c*(127+64);
// }

// void draw_julia()
// {
//     int i,j,n;
//     unsigned char r,g,b;
//     // Complex C = make_complex(0.32, 0.043);
//     float x,y;
//     for(i=0;i<DIMW;i+=1)
//     {
//         for(j=0;j<DIMW;j+=1)
//         {
//             x = ((float(i))/DIMW)*3 - 1.5;
//             y = ((float(j))/DIMW)*3 - 1.5;
//             n = suite_de_julia( 2, max_iter, make_complex(x,y), C );
//             couleur_julia( n, r,g,b);
//             put_pixel(i,j,r,g,b);
//         }
//     }
// }

int suite_de_julia(float borne, int max_it, Complex z0, Complex C) {
    for (int i=0; i<max_it; i++) {
        z0 = (z0*z0) + C;
        if (z0.re*z0.re+z0.im*z0.im > borne)
            return i;
    }
    return max_it;
}

void couleur_julia(float& r, float& g, float& b, int iter) {
    r,g,b = 0,0,0;
    r = (float)iter/(float)max_iter;
    g, b = (1-r);
}

void draw_julia() {
    const float scale = 1.0/DIMW*2;
    for (int i=0; i<DIMW; i++) {
        for (int j=0; j<DIMW; j++) {
            Complex z0 = make_complex(  (i*scale-1.0)*scale_factor,
                                        (j*scale-1.0)*scale_factor);
            int iter = suite_de_julia(limite, max_iter, z0, C);
            if (iter < max_iter) {
                float r,g,b;
                couleur_julia(r,g,b,iter);
                put_pixel(i,j,r*255,g*255,b*255);
            }
        }
    }
}

void update() {

    if (isKeyPressed(SDLK_LEFT))
        C.re += 0.05;
    if (isKeyPressed(SDLK_RIGHT))
        C.re -= 0.05;
    
    if (isKeyPressed(SDLK_UP))
        C.im += 0.05;
    if (isKeyPressed(SDLK_DOWN))
        C.im -= 0.05;
    
    if (isKeyPressed(SDLK_z))
        max_iter++;
    if (isKeyPressed(SDLK_s))
        max_iter = max(1, max_iter+1);
    
    if (isKeyPressed(SDLK_e))
        scale_factor += 0.2;
    if (isKeyPressed(SDLK_d))
        scale_factor = max(0.0,scale_factor-0.2);

    if (isKeyPressed(SDLK_r))
        limite += 0.1;
    if (isKeyPressed(SDLK_f))
        limite = max(0.0,limite-0.1);
}

int main(int, char**) {

    bool stop = false;
    winInit("julia", DIMW, DIMW);
    backgroundColor(0,0,0);
    setKeyRepeatMode(true);
    while (!stop) {
        winClear();
        update();
        draw_julia();
        stop = winDisplay();
    }
    winQuit();

    return 0;
}