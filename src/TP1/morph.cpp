#include "Grapic.h"
#include "point.h"

using namespace std;
using namespace grapic;

const int DIMW  = 500;
const int MAX   = 255;

struct Polygon {
    int n;
    Point p[MAX];
};

#include "poly1.h"
#include "poly2.h"
#include "poly3.h"
#include "poly4.h"

void polygon_add(Polygon& p, float px, float py) {
    if (p.n < MAX) {
        p.p[p.n] = make_point(px, py);
        p.n++;
    }
}

void polygon_draw(Polygon p) {
    color(255,255,255);
    for (int i=0; i<p.n; i++) {
        Point end;
        if (i==p.n-1)
            end = p.p[0];
        else
            end = p.p[i+1];
        
        Point start = p.p[i];
        line(start.x, start.y, end.x, end.y);
    }
}

Point point_interp(Point a, Point b, float t) {
    return (1-t)*a + t*b;
}


Polygon poly_interp(Polygon a, Polygon b, float t) {
    Polygon res;
    res.n = 0;
    if (a.n == b.n) {
        for (int i=0; i<a.n; i++) {
            Point comp = point_interp(a.p[i], b.p[i], t);
            polygon_add(res, comp.x, comp.y);
        }
    }
    return res;
}

Point center_of_mass(Polygon p) {
    Point res = {0,0};
    for (int i=0; i<p.n; i++)
        res = res+p.p[i];
    return (1/p.n)*res;
}

void polygon_scale(Polygon& p, float cx, float cy, float lambda) {
    Point center = make_point(cx, cy);
    for (int i=0; i<p.n; i++) {
        p.p[i] = lambda*(p.p[i]-center)+center;
    }   
}

void polygon_rotate(Polygon& p, float cx, float cy, float theta) {
    Point center = make_point(cx, cy);
    Point angle  = make_point_exp(1, theta);
    for (int i=0; i<p.n; i++) {
        p.p[i] = (p.p[i]-center)*angle+center;
    }   
}

void polygon_symmetry(Polygon& p, float cx, float cy, float theta) {
    Point center = center_of_mass(p);
    for (int i=0; i<p.n; i++) {
        p.p[i] = -1*(p.p[i]-center)+center;
    }   
}

void update(Polygon p1, Polygon p2, Polygon& p3) {
    float t = (sin(elapsedTime())+1)/2;
    p3 = poly_interp(p1,p2,t);
}

int main(int, char**) {
    winInit("morph", DIMW, DIMW);
    bool quit = false;
    Polygon p1,p2,p3;
    initPoly1(p1);
    initPoly4(p2);

    backgroundColor(0,0,0);
    while (!quit) {
        winClear();
        update(p1,p2,p3);
        polygon_draw(p3);
        quit = winDisplay();
    }
    pressSpace();
    winQuit();
}