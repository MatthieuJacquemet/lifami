#include "Grapic.h"
#include "_color.h"

using namespace std;
using namespace grapic;

const int DIMW  = 500;

void update(Image img1, Image img2, Image& img3) {
    float t = (sin(elapsedTime())+1)/2;
    for (int i=0; i<DIMW; i++) {
        for (int j=0; j<DIMW; j++) {
            Color a,b,c;
            a.r = image_get(img1, i,j, 2);
            a.g = image_get(img1, i,j, 1);
            a.b = image_get(img1, i,j, 0);

            b.r = image_get(img2, i,j, 2);
            b.g = image_get(img2, i,j, 1);
            b.b = image_get(img2, i,j, 0);

            c = color_lerp(a,b, t);
            image_set(img3, i, j, c.r, c.g, c.b, 255);
        }
    }
}

int main(int, char**) {
    winInit("image morph", DIMW, DIMW);
    bool quit = false;
    Image img1 = image("data/lena.png");
    Image img2 = image("data/lena2.png");
    Image img3 = image(DIMW, DIMW);

    backgroundColor(0,0,0);
    while (!quit) {
        winClear();
        update(img1,img2,img3);
        image_draw(img3,0,0);
        quit = winDisplay();
    }
    pressSpace();
    winQuit();
}