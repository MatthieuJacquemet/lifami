#include "Grapic.h"

#include <math.h>
#include <iostream>

#include "_complex.h"

using namespace std;
using namespace grapic;

const int DIMW = 500;

void draw() {
    color(255,0,0);

    for (int i=0; i<DIMW; i+=10){
        for (int j=0; j<DIMW; j+=10) {
            Complex pt = make_complex(i, j);
            circleFill(pt.re,pt.im, 1);
        }
    }

    // for (int r=0; r<DIMW/2; r+=DIMW/20){
    //     for (float theta=0; theta<360; theta+=360/10) {
    //         Complex pt = make_complex_exp(r,theta);
    //         circleFill(pt.re+DIMW/2,pt.im+DIMW/2,3);
    //     }
    // }
}

int main(int, char**) {

    winInit("Gride", DIMW, DIMW);
    backgroundColor(0,0,0);

    draw();
    winDisplay();
    pressSpace();
    winQuit();

    return 0;
}