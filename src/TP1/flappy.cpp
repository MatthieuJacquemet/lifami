#include "Grapic.h"

#include <math.h>
#include <ctime>
#include <iostream>
#include <algorithm>

#include "_complex.h"

using namespace std;
using namespace grapic;

const int DIMW = 500;
const int WING_R = 50;
const int G = 9.81;
float last = 0;
float y_vel = 0;

struct Bird {
    Complex position;
    float angle;
};

void init(Bird& bi) {
    bi.position = {DIMW/2,DIMW/2};
    bi.angle = 0;
}

void draw(Bird bi) {
    int x = bi.position.re;
    int y = bi.position.im;
    color(0,0,0);
    circleFill(x, y, 4);
    int wing_x = cos(bi.angle*M_PI/360)*WING_R;
    int wing_y = sin(bi.angle*M_PI/360)*WING_R;
    line(x, y, x-wing_x, y+wing_y);
    line(x, y, x+wing_x, y+wing_y);
}

void update(Bird& bi) {
    
    float dt = elapsedTime() - last;
    last = elapsedTime();

    y_vel -= G;

    if (isKeyPressed(SDLK_UP)) {
        bi.angle = sin(elapsedTime()*20)*20;
        y_vel = min(30000.0, y_vel+25.0);
    }
    bi.position.im += y_vel*0.005*dt;

    if (isKeyPressed(SDLK_LEFT)) {
        bi.position.re -= 0.04;
        bi.angle = sin(elapsedTime()*10)*20;
    }
    if (isKeyPressed(SDLK_RIGHT)) {
        bi.position.re += 0.04;
        bi.angle = sin(elapsedTime()*10)*20;
    }

}

int main(int, char**) {

    int a = 12;

    bool stop = false;
    winInit("Birds", DIMW, DIMW);
    backgroundColor(100,50,200);
    Bird bi;
    init(bi);

    setKeyRepeatMode(true);

    while (!stop) {
        winClear();
        draw(bi);
        update(bi);
        stop = winDisplay();
    }
    winQuit();

    return 0;
}