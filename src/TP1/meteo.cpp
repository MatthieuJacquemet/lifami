#include "Grapic.h"
#include "_color.h"

using namespace std;
using namespace grapic;

const int DIMW  = 800;
const int DIMH  = 500;
const int MAX = 32;

struct WeatherStation {
    float lon, lat;
    int x, y;
    float temperature;
};

struct WorldTemperature {
    int n;
    WeatherStation ws[MAX];
};

#include "temperatureInit.h"

int lat_to_pixel(float lon) {
    return (lon+90)/180 * DIMH;
}

int lon_to_pixel(float lat) {
    return (lat+180)/360 * DIMW;
}

float inverse_distance_weighting(WorldTemperature wt, int x, int y) {
    float wS = 0;
    float tS = 0;
    for (int i=0; i<wt.n; i++) {
        float d = (pow(wt.ws[i].x-x,2) + pow(wt.ws[i].y-y,2));
        if (d == 0)
            return wt.ws[i].temperature;
        else {
            float w = 1/d;
            float t = wt.ws[i].temperature*w;
            tS += t;
            wS += w;
        }
    }
    return tS/wS;
}

void show_map(WorldTemperature wt) {
    Image map = image("data/world.png");
    image_draw(map,0,0,DIMW, DIMH);
    color(255,0,0);
    for (int i=0; i<wt.n; i++) {
        wt.ws[i].x = lon_to_pixel(wt.ws[i].lon);
        wt.ws[i].y = lat_to_pixel(wt.ws[i].lat);
        circleFill(wt.ws[i].x,wt.ws[i].y,3);
    }
    float avg_temps[DIMH] = {0};
    float prev = 0;
    for (int i=0; i<DIMW; i++) {
        for (int j=0; j<DIMH; j++) {
            float temp = inverse_distance_weighting(wt,i,j);
            float t = (temp+27)/(33.717+27);
            avg_temps[j] += t/DIMW;
            Color col = color_lerp({0,0,255},{255,0,0}, t);
            color(col.r,col.g,col.b, 100);
            circleFill(i,j,1);

        }
    }
    color(0,255,0);
    for (int i=1; i<DIMH; i++) {
        line(avg_temps[i-1]*50, i-1, avg_temps[i]*50, i);
    }
}


int main(int, char**) {
    winInit("meteo", DIMW, DIMH);
    WorldTemperature wt;
    initFromData(wt);
    // winClear();
    show_map(wt);
    winDisplay();
    pressSpace();
    winQuit();
}