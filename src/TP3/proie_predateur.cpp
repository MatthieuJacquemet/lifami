#include <Grapic.h>
#include <iostream>

using namespace std;
using namespace grapic;

const int DIMW = 500;

struct Individu {
    bool state;
};

struct Ecosysteme {
    float nb_proie, nb_preda;
    float alpha, beta, gamma, delta;
    Image images[3];
};

void evolution_ecosysteme(Ecosysteme& eco, Plot& p) {

    float nb_preda = eco.nb_preda;
    float nb_proie = eco.nb_proie;
    eco.nb_proie = nb_proie * (1 + eco.alpha - eco.beta*nb_preda);
    eco.nb_preda = nb_preda * (1 - eco.gamma + eco.delta*nb_proie);

    float x = elapsedTime()*20;

    p.add(x, eco.nb_preda, 0);
    p.add(x, eco.nb_proie, 1);
    plot_draw(p,10,10,DIMW-10, DIMW-10);
}

void init_ecosystem(Ecosysteme& eco) {

    eco.alpha = 0.045f;
    eco.beta  = 0.001f;
    eco.gamma = 0.025f;
    eco.delta = 0.0002f;
    eco.nb_proie = 300;
    eco.nb_preda = 40;
}


int main(int, char**) {
    winInit("Game of life", DIMW, DIMW);
    backgroundColor(0,0,0);
    bool quit = false;
    Plot p;
    p.setSize(200);
    Ecosysteme eco;
    init_ecosystem(eco);
    color(255,255,255);
    while(!quit) {
        winClear();
        evolution_ecosysteme(eco, p);
        quit = winDisplay();
        delay(1000/60);
    }
}