#include <Grapic.h>
#include <iostream>
#include <random>
#include <vec2.h>

using namespace std;
using namespace grapic;

const int MAX_X = 200;
const int MAX_Y = 200;
const int DIM = 100;

const int DIMW = 800;

struct Cellule {
    bool state;
};

struct JeuDeLaVie {
    Cellule cells[MAX_X][MAX_Y];
    int dx, dy;
    int alive, dead;
};

void init(JeuDeLaVie& jeu) {
    jeu.dx = DIM;
    jeu.dy = DIM;

    jeu.alive = (jeu.dx*jeu.dy) / 5;
    jeu.dead = (jeu.dx*jeu.dy) - jeu.alive;

    for (int i=0; i<jeu.dx; i++) {
        for (int j=0; j<jeu.dy; j++)
            jeu.cells[i][j].state = false;
    }
}

void etat_initial(JeuDeLaVie& jeu) {

    int x,y;
    for (int i=0; i<jeu.alive; i++) {
        do {
            x = (rand()%(jeu.dx-2)) + 1;
            y = (rand()%(jeu.dy-2)) + 1;
        } while (jeu.cells[x][y].state);
        
        jeu.cells[x][y].state = true;
    }
}

void draw(JeuDeLaVie& jeu) {

    grid(0, 0, DIMW, DIMW, jeu.dx, jeu.dy);

    int size_x = DIMW/jeu.dx;
    int size_y = DIMW/jeu.dy;

    for (int i=0; i<jeu.dx; i++) {
        for (int j=0; j<jeu.dy; j++) {
            if (jeu.cells[i][j].state) {
                rectangleFill(size_x*i, size_y*j,size_x*(i+1),size_y*(j+1));
            }
        }
    }

}

void etat_suivant(JeuDeLaVie& jeu) {

    JeuDeLaVie jeu2 = jeu;

    for (int i=1; i<jeu.dx-1; i++) {
        for (int j=1; j<jeu.dy-1; j++) {
            int alive = 0;
            for (int x=i-1; x<=i+1; x++){
                for (int y=j-1; y<=j+1; y++) {
                    if (jeu2.cells[x][y].state && ((x!=i) || (y!=j)))
                        alive++;
                }
            }
            if (jeu.cells[i][j].state) {
                if (alive<2 || alive >= 4)
                    jeu.cells[i][j].state = false;
            } else if (alive == 3) 
                jeu.cells[i][j].state = true;
        }
    }

} 

bool update(JeuDeLaVie& jeu) {
    if (isMousePressed(SDL_BUTTON_LEFT)) {
        int x, y;
        mousePos(x,y);
        jeu.cells[int((float(x)/DIMW)*jeu.dx)][int((float(y)/DIMW)*jeu.dy)].state = true;
        return true;
    }
    return false;
}

int main(int, char**) {
    winInit("Game of life", DIMW, DIMW);
    backgroundColor(255,255,255);
    bool quit = false;
    bool _init_draw = true;

    JeuDeLaVie jeu;
    init(jeu);
    // etat_initial(jeu);
    setKeyRepeatMode(true);

    while(!quit) {
        winClear();
        draw(jeu);
        if (isKeyPressed(SDLK_SPACE))
            _init_draw = false;

        bool res = update(jeu);
        if (res)
            _init_draw = true;

        if (!_init_draw)
            etat_suivant(jeu);
        // delay(1.0f/30);
        quit = winDisplay();
        delay(1000/20);
    }
}