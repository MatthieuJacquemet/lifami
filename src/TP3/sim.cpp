#include <Grapic.h>
#include <ctime>

using namespace std;
using namespace grapic;

const int MAX_X = 50;
const int MAX_Y = 50;
const int DIMW  = 500;

const int GRASS     = 0;
const int PREY      = 1;
const int PREDATOR  = 2;
const int MUD       = 3;

const int PREDATOR_DIETTIME = 16;
const int PREDATOR_LIFETIME = 17;
const int PREY_DIETTIME     = 12;
const int PREY_LIFETIME     = 15;

struct Cell {
    int state;
    int lifetime;
    int diettime;
    int ground;
};

struct Ecosystem {

    Cell grid[MAX_X][MAX_Y];
    int size_x, size_y;
    int predator, prey;
    int elapsed_cycle;
    Image img[3];
};

Cell make_cell(Ecosystem& eco, int x, int y, int animal=0) {

    Cell c;
    c.state = animal;

    if (animal == PREY) {
        c.diettime  = PREY_DIETTIME;
        c.lifetime  = PREY_LIFETIME;
        c.ground    = MUD;
    }

    else if (animal == PREDATOR) {
        c.diettime  = PREDATOR_DIETTIME;
        c.lifetime  = PREDATOR_LIFETIME;
        c.ground    = eco.grid[x][y].ground;
    }

    else
        c.ground    = eco.grid[x][y].ground;

    return c;
}

void init_ecosystem(Ecosystem& eco) {

    eco.img[0] = image("data/grass.png");
    eco.img[1] = image("data/rabbit.png");
    eco.img[2] = image("data/wolf.png");
    eco.img[3] = image("data/mud.jpeg");

    eco.size_x = 20;
    eco.size_y = 20;

    eco.prey = 17;
    eco.predator = 9;

    for (int i=0; i<eco.size_x; i++) {
        for (int j=0; j<eco.size_y; j++) {
            eco.grid[i][j] = make_cell(eco,i,j);
            eco.grid[i][j].ground = GRASS;
        }
    }

    for (int i=0; i<eco.prey; i++) {
        int x,y;
        do {
            x = (rand()%(eco.size_x-1)) + 1;
            y = (rand()%(eco.size_y-1)) + 1;
        } while (eco.grid[x][y].state != 0);
        eco.grid[x][y] = make_cell(eco, x, y, PREY);
    }

    for (int i=0; i<eco.predator; i++) {
        int x,y;
        do {
            x = (rand()%(eco.size_x-1)) + 1;
            y = (rand()%(eco.size_y-1)) + 1;
        } while (eco.grid[x][y].state != 0);
        eco.grid[x][y] = make_cell(eco, x, y, PREDATOR);
    }
}

void draw_ecosystem(Ecosystem& eco) {

    int size_w = DIMW/eco.size_x;
    int size_h = DIMW/eco.size_y;

    for (int i=0; i<eco.size_x; i++) {
        for (int j=0; j<eco.size_y; j++) {

            image_draw(eco.img[eco.grid[i][j].ground],size_w*i,size_h*j,size_w, size_h);

            if (eco.grid[i][j].state != 0)
                image_draw(eco.img[eco.grid[i][j].state],size_w*i,size_h*j,size_w, size_h);
        }
    }
}

bool is_in_border(Ecosystem& eco, int x, int y) {

    return (x==0 || x==(eco.size_x -1) || y==0 || y==(eco.size_y -1));
}

void update_ecosystem(Ecosystem& eco) {


    Ecosystem eco2 = eco;

    for (int i=0; i<eco.size_x; i++) {
        for (int j=0; j<eco.size_y; j++) {

            int nb_prey = -1;
            int nb_predator = -1;
            int nb_empty = 0;

            int empty_x, empty_y;
            
            if (eco2.grid[i][j].state == PREY) {

                for (int x=i-1; x<=i+1; x++) {
        
                    for (int y=j-1; y<=j+1; y++) {

                        if (eco2.grid[x][y].state == PREY)
                            nb_prey++;     

                        if (eco2.grid[x][y].state == 0 && !is_in_border(eco, x,y))
                        {
                            nb_empty++;
                            empty_x = x;
                            empty_y = y;          
                        }        
                    }
                }
                if (nb_prey > 0 and nb_empty > 0)
                    eco.grid[empty_x][empty_y] = make_cell(eco,empty_x,empty_y,PREY);
            }
            else if (eco2.grid[i][j].state == PREDATOR) {
                
                nb_prey = 0;
                nb_predator -1;
                nb_empty = 0;
                int prey_x, prey_y;

                for (int x=i-1; x<=i+1; x++) {
        
                    for (int y=j-1; y<=j+1; y++) {

                        if (eco2.grid[x][y].state == PREDATOR)
                            nb_predator++;

                        if (eco2.grid[x][y].state == PREY) {
                            nb_prey++;
                            prey_x = x;
                            prey_y = y;
                        }

                        if (eco2.grid[x][y].state == 0 && !is_in_border(eco, x,y)) {
                            nb_empty++;
                            empty_x = x;
                            empty_y = y;          
                        }   
                    }
                }
                if (nb_prey > 0) {
                    eco.grid[prey_x][prey_y] = make_cell(eco,prey_x,prey_y);
                    eco.grid[i][j].diettime = PREDATOR_DIETTIME;
                }
                if (nb_predator > 0 && nb_empty > 0)
                    eco.grid[empty_x][empty_y] = make_cell(eco,empty_x,empty_y,PREDATOR);
            }
        }
    }
    for (int i=0; i<eco.size_x; i++) {
        for (int j=0; j<eco.size_y; j++) {
            if (eco.grid[i][j].state != 0) {
                eco.grid[i][j].lifetime--;
                eco.grid[i][j].diettime--;
            }
            if (eco.grid[i][j].lifetime == 0 || eco.grid[i][j].diettime == 0) {
                eco.grid[i][j] = make_cell(eco,i,j);
                cout << eco.grid[i][j].state <<" " << eco.elapsed_cycle << endl;
            }
        }
    }
    eco.elapsed_cycle++;
}
int main(int, char**) {

    winInit("rabbit hole", DIMW, DIMW);
    backgroundColor(0,0,0,0);

    srand(time(NULL));

    bool quit = false;

    Ecosystem eco;
    init_ecosystem(eco);
    eco.elapsed_cycle = 0;

    while (!quit) {

        winClear();

        delay(500);

        update_ecosystem(eco);
        draw_ecosystem(eco);

        quit = winDisplay();
    }
}