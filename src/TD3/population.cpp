#include <iostream>
#include "Grapic.h"
#include <random>

using namespace std;
using namespace grapic;

const int MAX = 12;
const int DIMW = 500;

struct Population {
    int mort[MAX];
    int naissance[MAX];
    int init_pop;
};

struct Hauteur {
    int n;
    float T[MAX];
};

int loups(Population p, int d) {
    int s = p.init_pop;
    for (int i=0; i<d; i++)
        s += p.naissance[i] - p.mort[i];
    return s;
}

float derive(Hauteur h, int j) {
    if (j>0 && j < h.n)
        return h.T[j] - h.T[j-1];
    else
        cout << "erreur" << endl;
    return 0;
}

int jours_pluie(Hauteur h) {
    int s = 0;
    for (int i=1; i<h.n; i++) {
        if (derive(h,i)>0) s++;
    }
    return s;
}

float moyenne_pluie(Hauteur h) {
    float r = 0;
    int   n = 0;
    for (int i=1; i<h.n; i++) {
        float d = derive(h,i);
        if (d>0) {
            r+=d;
            n++;
        }
    }
    if (n>0)
        return r/n;
    return 0;
}


float forte_pluie(Hauteur h) {
    float n = 0;
    float m = moyenne_pluie(h);
    for (int i=1; i<h.n; i++) {
        if (derive(h,i)>m) {
            n++;
        }
    }
    return n;
}

void init_hauteur(Hauteur h) {
    h.n = 
}

int main(int, char**) {

    srand(NULL);

    winInit("derive", DIMW, DIMW);
    Plot f,d;
    Hauteur h;

    float t;
    backgroundColor(0,0,0);
    int prev = (rand()%40)-10;
    plot_add(f,0,prev);
    for (int i=0; i<40; i++) {
        int value = prev + (rand()%20)-10;
        prev = value;
        plot_add(f, i, value);
        // plot_add(d, i, derive)
    }

    plot_draw(f, 0,0,DIMW,DIMW);
    winDisplay();
    pressSpace();
    return 0;
}