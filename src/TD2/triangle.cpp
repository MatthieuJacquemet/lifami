#include "Grapic.h"
#include <iostream>
#include "_color.h"
#include "_complex.h"
#include <malloc.h>

using namespace std;
using namespace grapic;

const int DIMW = 700;

struct Vertex {
    Complex position;
    Color   color;
};

struct Triangle {
    Vertex v0, v1, v2;
};

void get_boundingbox(float& xmin, float& ymin, float& xmax, float& ymax,
                        Triangle tri) {
    Complex data[3] = {tri.v0.position,tri.v1.position,tri.v2.position};
    xmin = DIMW, ymin = DIMW;
    xmax = 0, ymax = 0;

    for (int i=0; i<3; i++) {
        if (data[i].re < xmin)
            xmin = data[i].re;
        if (data[i].re > xmax)
            xmax = data[i].re;
        if (data[i].im < ymin)
            ymin = data[i].im;
        if (data[i].im > ymax)
            ymax = data[i].im;
    }
}

void draw_triangle(Triangle tri) {

    Complex A = tri.v0.position;
    Complex B = tri.v1.position;
    Complex C = tri.v2.position;

    float xmin, ymin, xmax, ymax;
    get_boundingbox(xmin, ymin, xmax, ymax, tri);

    // float triangle_area = module((B-A)*(C-A));

    for (int i=xmin; i<=xmax; i++) {
        for (int j=ymin; j<=ymax; j++) {
            Complex P = {i,j};
            Complex PmC = P-C;
            float triangle_area=(B.im-C.im)*(A.re-C.re)+(C.re-B.re)*(A.im-C.im);
            float u = ((B.im-C.im)*PmC.re+(C.re-B.re)*PmC.im)/triangle_area;
            float v = ((C.im-A.im)*PmC.re+(A.re-C.re)*PmC.im)/triangle_area; 
  //(module((A-C)*(P-C))/triangle_area); 
            float w = 1-u-v;
            if (u>=0 && v>=0 && w>=0) {
                Color pixel = u*tri.v0.color + v*tri.v1.color + w*tri.v2.color;
                put_pixel(i, j, pixel.r, pixel.g, pixel.b);
            }
            // else put_pixel(i,j, 255,255,255);
        }
    }
}

void update(Triangle& tri) {
    if (isKeyPressed(SDLK_LEFT))
        tri.v0.position.re -= 5;
    if (isKeyPressed(SDLK_RIGHT))
        tri.v0.position.re += 5;
    if (isKeyPressed(SDLK_UP))
        tri.v0.position.im += 5;
    if (isKeyPressed(SDLK_DOWN))
        tri.v0.position.im -= 5;
}

int main(int, char**) {

    Triangle tri = {{{70,100},{255,0,0}},
                    {{260,450},{0,255,0}},
                    {{400,30},{0,0,255}}};
    
    winInit("test", 500, 500);
    bool quit = false;
    setKeyRepeatMode(true);
    backgroundColor(0,0,0);
    while(!quit) {
        update(tri);
        draw_triangle(tri);
        quit = winDisplay();
        winClear();
    }
    // pressSpace();
    winQuit();

}