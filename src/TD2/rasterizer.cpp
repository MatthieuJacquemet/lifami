#include <Grapic.h>
#include <glm.hpp>

#define GLM_ENABLE_EXPERIMENTAL

#include <gtx/transform.hpp>
#include <iostream>
#include <string.h>

using namespace std;
using namespace glm;
using namespace grapic;

const int DIMW = 700;

struct Vertex{
    vec3 pos;
    vec2 uv;
    uint8_t r,g,b;
    Vertex(vec3 vert_pos) : pos(vert_pos) {};
    Vertex() {};
};

struct Triangle{
    unsigned int indices[3];
    vec3 normal;
    Triangle(unsigned int a, unsigned int b, unsigned int c) : indices{a,b,c}{}; 
};

struct BoundingBox{
    int max_x, min_x;
    int max_y, min_y;
};


void get_bounding(vec4 tri[3], BoundingBox* bb)
{
    int max_x = 0, max_y = 0, min_x = 0, min_y = 0;
    for (int i=0; i<3; i++){
        if (tri[i].x > tri[max_x].x) max_x = i;
        if (tri[i].y > tri[max_y].y) max_y = i;
        if (tri[i].x < tri[min_x].x) min_x = i;
        if (tri[i].y < tri[min_y].y) min_y = i;
    }
    bb->max_x = max_x;
    bb->max_y = max_y;
    bb->min_x = min_x;
    bb->min_y = min_y;
}

inline float map(float x, float in_min, float in_max, float out_min, float out_max){
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void print_matrix(mat4 m){
    // system("cls");
    cout<<m[0].x<<" "<<m[1].x<<" "<<m[2].x<<" "<<m[3].x<<endl;
    cout<<m[0].y<<" "<<m[1].y<<" "<<m[2].y<<" "<<m[3].y<<endl;
    cout<<m[0].z<<" "<<m[1].z<<" "<<m[2].z<<" "<<m[3].z<<endl;
    cout<<m[0].w<<" "<<m[1].w<<" "<<m[2].w<<" "<<m[3].w<<endl;
}

void print_matrix(mat3 m){
    // system("cls");
    cout<<m[0].x<<" "<<m[1].x<<" "<<m[2].x<<endl;
    cout<<m[0].y<<" "<<m[1].y<<" "<<m[2].y<<endl;
    cout<<m[0].z<<" "<<m[1].z<<" "<<m[2].z<<endl;
}

template<class T>
    class Buffer
    {
        public:
        T* _buffer;
        size_t _buffer_size;
        int size_x, size_y;
        int _offset_x, _offset_y;

        Buffer() {};
 
        Buffer(int x, int y) : size_x(x), size_y(y), _buffer_size(x*y*sizeof(T))
        {
            T* loc = new T[_buffer_size];
            if (loc == nullptr)
                cout << "error : could not allocate image buffer" << endl;
            else
                _buffer = (T*)loc;
        }

        T* operator[] (int i) const{
            return &_buffer[std::min(i,size_x)*size_y];}

        T& operator=(const T value){
            (*this)[_offset_x][_offset_y] = value;
            return (*this)[_offset_x][_offset_y];}

        void fill(T value){
            for (unsigned int i=0; i<size_x*size_y; i++)
                _buffer[i] = value;
        }
    };

struct RGB8{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    RGB8() {};
    RGB8(uint8_t red, uint8_t green, uint8_t blue) : r(red), g(green), b(blue) {}
    RGB8(vec3 value) :  r(std::min(1.0f, std::max(0.0f,value.r))*255),
                        g(std::min(1.0f, std::max(0.0f,value.g))*255),
                        b(std::min(1.0f, std::max(0.0f,value.b))*255) {}
    // RGB8 operator+(RGB8 value) {
    //     return {value.r+r,value.g+g,value.b+b};
    // }
    // RGB8 operator-(RGB8 value) {
    //     return {value.r-r,value.g-g,value.b-b};
    // }
    // RGB8 operator*(RGB8 value) {
    //     return {value.r*r,value.g*g,value.b*b};
    // }
    // RGB8 operator/(RGB8 value) {
    //     return {value.r/r,value.g/g,value.b/b};
    // }
    // RGB8 operator*(float value) {
    //     return {value*r,value*g,value*b};
    // }
    // RGB8 operator/(float value) {
    //     return {r/value,g/value,b/value};
    // }
};

//void loadMesh(string path, vector<Triangle>* tri, vector<vec3>)
template<class T>
struct Mesh{
    std::vector<T>* vertices;
    std::vector<Triangle>* indices;
    size_t _attrib_size;
    Mesh(std::vector<T>* verts, std::vector<Triangle>* index) : _attrib_size(sizeof(T)),
                                                                vertices(verts),
                                                                indices(index) {}
};

class Transform
{   
    public:
        vec3 position;
        vec3 rotation;
        vec3 scaling;

        mat4 scale_matrix;
        mat4 orientation_matrix;
        mat4 translation_matrix;
        mat4 transform_matrix;

        Transform(vec3 pos=vec3(0.0f), vec3 rot=vec3(0.0f), vec3 sca=vec3(1.0f)) : position(pos), rotation(rot), scaling(sca)
        {
            translation_matrix = glm::translate(position);
            orientation_matrix = glm::rotate(rot.x, vec3(1.0f,0.0f,0.0f));
            scale_matrix = glm::scale(scaling);
            transform_matrix = translation_matrix * orientation_matrix * scale_matrix;
        }


        void set_translation(float x, float y, float z){
            set_translation(vec3(x,y,z));
        }

        void set_translation(vec3 offset){
            position = offset;
            translation_matrix = glm::translate(offset);
        }

        void set_rotation(float angle, int axis){
            vec3 rot = vec3(0.0f);
            rot[axis] = 1;
            orientation_matrix = glm::rotate(angle, rot);
            rotation = rot * angle;
        }

        void set_scale(float x, float y, float z){
            scale(vec3(x,y,z));
            
        }

        void set_scale(vec3 offset){
            scaling = offset;
            scale_matrix = glm::scale(offset);
        }

        void compute_transform(void){
            transform_matrix = translation_matrix * orientation_matrix * scale_matrix;
            //return transform_matrix;
        }

        void translate(vec3 pos, bool local=false) {
            if (local) {
                pos = mat3(orientation_matrix)*pos;
                translation_matrix = glm::translate(translation_matrix, pos);
            } else
                translation_matrix = glm::translate(translation_matrix, pos);
            position += pos;

        }

        void rotate(float angle, vec3 axis, bool local=false) {
            if (local) {
                orientation_matrix = glm::rotate(orientation_matrix, angle, axis);
            } else {
                orientation_matrix = glm::rotate(angle, axis)*orientation_matrix;
            }
            rotation += angle*axis;
        }

        void rotate(float angle, int axis, bool local=false) {
            vec3 rot = vec3(0.0f);
            rot[axis] = 1;
            rotate(angle, rot, local);
        }
};

class Camera : public Transform
{
    public:
        float size_x, size_y;
        float aspect_ratio;
        float fov;
        float near;
        float far;

        mat4 projection_matrix;
        mat4 camera_matrix;
        mat4 view_matrix;

        Camera( float fov, float size_x, float size_y,
                vec3 pos=vec3(0.0f), vec3 rot=vec3(0.0f), 
                float near=0.01f, float far=1000.0f) :   Transform(pos, rot),
                                                        size_x(size_x),
                                                        size_y(size_y),
                                                        aspect_ratio(size_x/size_y),
                                                        fov(fov),
                                                        near(near),
                                                        far(far)
        {
            projection_matrix = perspectiveFov(fov, size_x, size_y, near, far);
            // view_matrix = translation_matrix * orientation_matrix;
            // camera_matrix = projection_matrix * view_matrix;
        }

        void compute_camera_matrix(void){
            view_matrix = glm::inverse(translation_matrix * orientation_matrix);
            camera_matrix = projection_matrix * view_matrix;
        }

        void look_at(vec3 direction, vec3 up){
            // view_matrix = glm::lookAt(position, direction, up);
            // orientation_matrix = mat3(view_matrix);
            camera_matrix = projection_matrix * view_matrix;
            
        }

        void update(float deltatime) {
            int mouse_x, mouse_y;
            mousePos(mouse_x, mouse_y);
            mouse_x -= DIMW/2;
            mouse_y -= DIMW/2;
            // cout << mouse_x << " " << mouse_y << endl;
            float move_speed = 15;
            // cout << (float)mouse_x/500.0*deltatime << endl;
            // cout << (float)mouse_x/100.0*deltatime << endl;
            rotate((float)-mouse_x/15.0*deltatime, vec3(0,0,1));
            rotate((float) mouse_y/15.0*deltatime, vec3(1,0,0), true);
            // set_translation((float)mouse_x/100,2);
            // set_translation((float)mouse_y/10,0);

            if (isKeyPressed(SDLK_UP) || isKeyPressed(SDLK_e))
                translate(vec3(0,0,-move_speed*deltatime),true);
            if (isKeyPressed(SDLK_DOWN) || isKeyPressed(SDLK_d)) 
                translate(vec3(0,0,move_speed*deltatime), true);
            if (isKeyPressed(SDLK_LEFT) || isKeyPressed(SDLK_s))
                translate(vec3(move_speed*-deltatime,0,0),true);
            if (isKeyPressed(SDLK_RIGHT) || isKeyPressed(SDLK_f)) 
                translate(vec3(move_speed*deltatime,0,0), true);
            
            SDL_WarpMouseInWindow((SDL_Window*)Grapic::singleton().window(), DIMW/2, DIMW/2-1);
        }
};

class Object : public Transform
{
    public:
        Mesh<uint8_t>* mesh;

        Object(Mesh<uint8_t>* mesh, vec3 pos=vec3(0.0f), vec3 rot=vec3(0.0f),
                vec3 scale=vec3(1.0f)) : Transform(pos, rot, scale), mesh(mesh) {}
};


void draw_buffer(Buffer<RGB8>* buffer, Image& img)
{
    RGB8* pixel;
    for (unsigned int x=0; x<buffer->size_x; x++){
        for (unsigned int y=0; y<buffer->size_y; y++){
            pixel = &(*buffer)[x][y];
            img.set(x,y,pixel->b,pixel->g, pixel->r,255);
            //put_pixel(x,y,pixel->r, pixel->g, pixel->b);
        }
    }
    image_draw(img, 0,0,DIMW, DIMW);
}

struct VertexAttributs{
    vec3 pos;
    VertexAttributs(float x,float y,float z) : pos(x,y,z) {};
};

struct attribs : public VertexAttributs{
    vec3 color;
    vec3 normal;
    vec2 uv;
    attribs(float x,float y,float z,
            float r=0,float  g=0, float b=0,
            float nx=0,float ny=0,float nz=0,
            float u=0,float v=0):   VertexAttributs(x,y,z),
                                    normal(vec3(nx,ny,nz)),
                                    color(vec3(r,g,b)),
                                    uv(vec2(u,v)) {};

};

enum DataType { CHAR=0x0u, SHORT, INT, LONG, UCHAR, USHORT, UINT, ULONG, FLOAT, DOUBLE,
                VEC2, VEC3, VEC4};

struct DataDesc{
    DataType data_type;
    void* data_address;
    DataDesc(DataType type, void* addr) : data_type(type), data_address(addr) {}
};

struct VaryingData{
    vector<DataDesc> layout;
};

template<class _Att_type>
    inline _Att_type interpolate(float z0, float z1, float q, float z, _Att_type c0, _Att_type c1){
        //float fact = (z-z0)/(z1-z0);
        //if (fact > 1) cout << fact << endl;
        //return (c0/z0 + q*(c1/z1 - c0/z0)) / (1/z0 + q*(1/z1 - 1/z0));
        //return (c0*(1-fact) + c1*fact);// / (1/z0*(1-fact) + 1/z1*fact);
        return z*(c0/z0*(1-q) + c1/z1*q);
    }

template<class _DataType>
    struct _Varying_Data{

        int _vert_index;
        _DataType value;
        _DataType values[3];
        _DataType min_value;
        _DataType max_value;

        inline void interpolate_data(  float min_z_a, float min_z_b, float max_z_a, float max_z_b,
                float q0, float q1, float qz0, float qz1, int min_a, int min_b, int max_a, int max_b){
            min_value    = interpolate<_DataType>(min_z_a, min_z_b, q0, qz0, values[min_a], values[min_b]);
            max_value    = interpolate<_DataType>(max_z_a, max_z_b, q1, qz1, values[max_a], values[max_b]);
        }
        _DataType& operator=(const _DataType value){
            values[_vert_index] = value;
            return values[_vert_index];
        }
    };


struct RenderTargetData{
    vector<Buffer<char>*> layout;
    inline virtual void set_pixel_index(int x, int y){}
};

struct MRT_target : public RenderTargetData{
    Buffer<RGB8> color;
    Buffer<vec3> normal;
    Buffer<vec2> uv;

    inline void set_pixel_index(int x, int y) {
        color._offset_x = x;
        color._offset_y = y;
        // normal._offset_x = x;
        // normal._offset_y = y;
        // uv._offset_x = x;
        // uv._offset_y = y;
    }
};

class Texture2D
{
    public:
        Image img;
        int size_x;
        int size_y;

        Texture2D(string path) {
            img = image(path.c_str());
            size_x = img.surface()->w;
            size_y = img.surface()->h;
        }

        inline vec3 sample(float x, float y)
        {   
            int coord_x=fmod(x,1.0f)*size_x;
            int coord_y=fmod(y,1.0f)*size_y;
            return vec3(img.get(coord_x, coord_y,0),
                        img.get(coord_x, coord_y,1),
                        img.get(coord_x, coord_y,2))/255.0f;
        }

        inline vec3 sample(vec2 coord) {
            return sample(coord.x, coord.y);
        }
};


class Light : public Transform {
    public:
    Light(): Transform() {};
    vec3 color;
    float energy;
    // Buffer<float> shadow_map;
};

template<class _Att_struct, class _Buffer_struct>
    class Shader
    {
        public:
            // __attribute__((fastcall))
            virtual vec4 vertexShader(_Att_struct* in);
            // __attribute__((fastcall))
            virtual void fragmentShader(_Buffer_struct* out);
            inline virtual void set_varying_index(int i);
            inline virtual void interpolate_h(float min_z,float max_z, float q, float z);
            inline virtual void interpolate_v(float min_z_a, float min_z_b, float max_z_a, float max_z_b,
                        float q0, float q1, float qz0, float qz1, int min_a, int min_b, int max_a, int max_b);
            size_t varying_size;
    };


template<class _Att_struct,class _Buffer_struct>
    class InterpolationShader : public Shader<_Att_struct, _Buffer_struct>
    {
        private: // interpolated data between vertex shader and fragment shader
            _Varying_Data<vec3> color;
            _Varying_Data<vec3> normal;
            _Varying_Data<vec3> pos;
            _Varying_Data<vec2> uv;

        public: // uniforms variables
            mat4 MVP;
            mat3 rotation_matrix;
            Texture2D* tex;
            //struct Light* point_light;
            vector<Light>* lights;
            mat4 model_matrix;
            vec3 view_pos;

            inline void set_varying_index(int i){
                uv._vert_index = i;
                normal._vert_index = i;
                pos._vert_index = i;
                color._vert_index = i;
            }
            inline void interpolate_v(float min_z_a, float min_z_b, float max_z_a, float max_z_b,
                    float q0, float q1, float qz0, float qz1, int min_a, int min_b, int max_a, int max_b){

                uv.interpolate_data(min_z_a, min_z_b, max_z_a, max_z_b, q0, q1, qz0, qz1, min_a, min_b, max_a, max_b);
                normal.interpolate_data(min_z_a, min_z_b, max_z_a, max_z_b, q0, q1, qz0, qz1, min_a, min_b, max_a, max_b);
                pos.interpolate_data(min_z_a, min_z_b, max_z_a, max_z_b, q0, q1, qz0, qz1, min_a, min_b, max_a, max_b);
                color.interpolate_data(min_z_a, min_z_b, max_z_a, max_z_b, q0, q1, qz0, qz1, min_a, min_b, max_a, max_b);
            }
            inline void interpolate_h(float min_z,float max_z, float q, float z){
                uv.value        = interpolate<vec2>(min_z, max_z, q, z, uv.min_value, uv.max_value);
                normal.value    = interpolate<vec3>(min_z, max_z, q, z, normal.min_value, normal.max_value);
                pos.value       = interpolate<vec3>(min_z, max_z, q, z, pos.min_value, pos.max_value);
                color.value     = interpolate<vec3>(min_z, max_z, q, z, color.min_value, color.max_value);
            }

            // __attribute__((fastcall))
            vec4 vertexShader(_Att_struct* in){
                color       = in->color;
                normal      = rotation_matrix*in->normal;
                pos         = vec3(model_matrix*vec4(in->pos, 1.0));
                uv          = in->uv;
                return MVP * vec4(in->pos, 1.0f);
            }
            
            // __attribute__((fastcall))
            void fragmentShader(_Buffer_struct* out){
                const float c1=0.2, c2=0.3;
                const float fill = 0.1f;
                const float roughness = 10.0f;
                //out->color = RGB8(tex->sample(uv.value*8.0f));
                //out->color  = RGB8(color.value);
                //out->color = RGB8(vec3(uv.value.x, uv.value.y, 0));
                vec3 view_vector = normalize(pos.value - view_pos);
                
                vec3 diff_acc = vec3(0.0);
                vec3 spec_acc = vec3(0.0);

                for (int i=0; i<lights->size(); i++){

                    vec3 light_vector = normalize((*lights)[i].position - pos.value);
                    vec3 reflect_vector = reflect(view_vector, normal.value);

                    float distance = light_vector.length();
                    float NdotL = std::max(dot(normal.value, light_vector), 0.0f);
                    float attenuation = 1.0f / (1.0 + c1*distance + c2*pow(distance,2));

                    diff_acc += (*lights)[i].color*(*lights)[i].energy*attenuation*NdotL;
                    spec_acc += (*lights)[i].color*pow(std::max(
                        dot(light_vector,reflect_vector), 0.0f), roughness)*0.1f
                        *(*lights)[i].energy;
                }
                // vec3 light_vector = normalize((*lights)[0].position - pos.value);
                //vec3 reflect_vector = reflect(view_vector, normal.value);
                // out->color = RGB8(vec3(pow(std::max(dot(light_vector,reflect_vector), 0.0f),roughness)));
                // out->color = RGB8(view_vector);
                out->color = RGB8(tex->sample(uv.value)*(diff_acc) + spec_acc);
                // out->color = RGB8(pos.value);
            }
    };

inline vec3 getTriangleNormal(vec4 vertices[3])
{
    vec3 edge_a, edge_b;
    edge_a = vec3(vertices[1]) - vec3(vertices[0]);
    edge_b = vec3(vertices[2]) - vec3(vertices[0]);
    return normalize(cross(edge_a, edge_b));
}

class RenderPipeline
{
    public:
        Buffer<float>* depth_buffer;
        RenderTargetData* render_target;
        Shader<void, void>* shader;
        Camera* active_camera;
        unsigned int size_x;
        unsigned int size_y;

        RenderPipeline(int x, int y) : size_x(x), size_y(y){
            depth_buffer = new Buffer<float>(size_x, size_y);
        }

        void draw_object(Object* obj){
            vec4 coord[3];
            void* var_addr;
            void* attrib_addr;
            int id;
            Triangle* cur_tri;
            vec3 normal;
            for (int tri_id=0; tri_id<obj->mesh->indices->size(); tri_id++){
                cur_tri = &(*obj->mesh->indices)[tri_id];
                bool clipped = false;
                for (int i=0; i<3; i++){
                    id = cur_tri->indices[i];        
                    attrib_addr = static_cast<void*>(&(*obj->mesh->vertices)[0] + id*obj->mesh->_attrib_size);
                    shader->set_varying_index(i);
                    coord[i] = shader->vertexShader(attrib_addr);
                    // triangle culling
                    if (coord[i].z < 0) {
                        clipped = true;
                        break;
                    }
                    //perspective divide
                    // coord[i].x /= coord[i].w;
                    // coord[i].y /= coord[i].w;
                    //coord[i].z /= coord[i].w;
                }
                normal = getTriangleNormal(coord);
                if (normal.z < 0 && !clipped) 
                    raster_triangle(coord, var_addr, normal);
            }
        }

        inline void raster_triangle(vec4 vert[3], void* in, vec3 color)
        {
            BoundingBox bb;
            get_bounding(vert, &bb);
            float height, min_x, max_x, min_y, max_y, coord = 0, coord_y, coord_x, dist,
                    min_ab_factor, max_ab_factor;
            vec3 edge, offset;
            min_y = vert[bb.min_y].y;
            max_y = vert[bb.max_y].y;
            int next_id = 0, min_edge_a = 0, min_edge_b = 0, max_edge_a = 0, max_edge_b = 0;
            float lambda = 0, vertical_lambda = 0, lambda_min = 0, lambda_max = 0, lambda1 = 0, lambda2 = 0, lambda3 = 0;
            float min_depth = 0, max_depth = 0, depth = 0;
            int max_line = std::max(0,int(floor(map(min_y, -1, 1, 0, size_y))));
            int max_column = 0;
            float min_a_fact, min_b_fact, max_a_fact, max_b_fact;

            
            for (int line=std::min(int(size_y),int(floor(map(max_y,-1,1,0,size_y)))); line>=max_line; line--){
                min_x =  1;
                max_x = -1;
                coord_y = map(line, 0, size_y, -1, 1);
                for (int i=0; i<3; i++){
                    next_id = (i+1)%3;
                    offset = vec3(vert[i]);
                    edge = vec3(vert[next_id]) - offset;
                    height = coord_y - offset.y;
                    if (height <= std::max(0.0f,edge.y) && height >= std::min(0.0f, edge.y)){
                        if (edge.y == 0){
                            lambda = 0;
                            coord = 0;
                        }else {
                            lambda = height/edge.y;
                            coord = lambda*edge.x;}
                        coord+=offset.x;
                        if (coord<min_x){
                            min_x = coord;
                            lambda_min = lambda;
                            min_edge_a = i;
                            min_edge_b = next_id;}
                        if (coord>max_x){
                            max_x = coord;
                            lambda_max = lambda;
                            max_edge_a = i;
                            max_edge_b = next_id;}
                        //put_pixel(ceil(map(coord,-1,1,0,size_x)),line, 255,0,0);
                        
                    }
                }
                
                min_depth = 1/(1/vert[min_edge_a].z*(1-lambda_min) + 1/vert[min_edge_b].z*lambda_min);
                max_depth = 1/(1/vert[max_edge_a].z*(1-lambda_max) + 1/vert[max_edge_b].z*lambda_max);

                max_column = int(floor(map(max_x,-1,1,0,size_x)));
                shader->interpolate_v(vert[min_edge_a].z, vert[min_edge_b].z, vert[max_edge_a].z, vert[max_edge_b].z,
                                        lambda_min, lambda_max, min_depth, max_depth,
                                        min_edge_a, min_edge_b, max_edge_a, max_edge_b);
                #pragma omp for
                for (int i=int(floor(map(min_x,-1,1,0,size_x))); i<=max_column; i++){

                    vertical_lambda = (map(i,0,size_x,-1,1)-min_x)/(max_x - min_x);
                    depth = 1/(1/min_depth*(1-vertical_lambda) + 1/max_depth*vertical_lambda);

                    if (depth < (*depth_buffer)[i][line]){ // depth test
                        (*depth_buffer)[i][line] = depth;
                        shader->interpolate_h(min_depth, max_depth, vertical_lambda, depth);
                        render_target->set_pixel_index(i, line);
                        shader->fragmentShader(render_target);
                        
                        //color = vec3(depth/10);
                        //put_pixel(i, line, color.r*255,color.g*255,color.b*255);
                    }

                }
            }
        }
};

// class Logger
// {
//     float raster_time;
//     float transform_time;

// };

void draw_image(Texture2D* src_img, Buffer<RGB8>* buffer)
{
    for (unsigned int x=0; x<buffer->size_x; x++){
        for (unsigned int y=0; y<buffer->size_y; y++)
            (*buffer)[x][y] = RGB8(src_img->sample(vec2(float(x)/buffer->size_x, float(y)/buffer->size_y)));
    }
}

int main(int, char**)
{

    bool quit = false;

    winInit("Software Rasterizer", DIMW, DIMW);
    backgroundColor(0,0,0);
    winClear();

    std::vector<attribs> vertex_buffer = {
    //attributs  x  y  z  r  g  b  nx ny nz u     v
        attribs( 1, 1,-1, 0, 1, 0, 0, 1, 0, 0.75, 2/3.0f),  //3 1
        attribs(-1, 1,-1, 1, 0, 1, 0, 1, 0, 1   , 2/3.0f),  //6 0
        attribs( 1, 1, 1, 1, 1, 1, 0, 1, 0, 0.75, 1/3.0f),  //2 12
        attribs(-1, 1, 1, 0, 1, 1, 0, 1, 0, 1   , 1/3.0f),  //  13
        attribs( 1,-1,-1, 1, 0, 0, 1, 0, 0, 0.5 , 2/3.0f),  //0 2
        attribs( 1, 1,-1, 0, 1, 0, 1, 0, 0, 0.75, 2/3.0f),  //3 1
        attribs( 1,-1, 1, 1, 1, 0, 1, 0, 0, 0.5 , 1/3.0f),  //1 11
        attribs( 1, 1, 1, 1, 1, 1, 1, 0, 0, 0.75, 1/3.0f),  //2 12
        attribs(-1,-1,-1, 0, 0, 0, 0,-1, 0, 0.25, 2/3.0f),  //7 5
        attribs( 1,-1,-1, 1, 0, 0, 0,-1, 0, 0.5 , 2/3.0f),  //0 2
        attribs(-1,-1, 1, 0, 0, 1, 0,-1, 0, 0.25, 1/3.0f),  //4 8
        attribs( 1,-1, 1, 1, 1, 0, 0,-1, 0, 0.5 , 1/3.0f),  //1 11
        attribs(-1, 1,-1, 1, 0, 1,-1, 0, 0, 0   , 2/3.0f),  //6 6
        attribs(-1,-1,-1, 0, 0, 0,-1, 0, 0, 0.25, 2/3.0f),  //7 5
        attribs(-1, 1, 1, 0, 1, 1,-1, 0, 0, 0   , 1/3.0f),  //5 7
        attribs(-1,-1, 1, 0, 0, 1,-1, 0, 0, 0.25, 1/3.0f),  //4 8
        attribs(-1, 1,-1, 1, 0, 1, 0, 0,-1, 0.25, 1),       //6 4
        attribs( 1, 1,-1, 0, 1, 0, 0, 0,-1, 0.5 , 1),       //3 3
        attribs(-1,-1,-1, 0, 0, 0, 0, 0,-1, 0.25, 2/3.0f),  //7 5
        attribs( 1,-1,-1, 1, 0, 0, 0, 0,-1, 0.5 , 2/3.0f),  //0 2
        attribs(-1,-1, 1, 0, 0, 1, 0, 0, 1, 0.25, 1/3.0f),  //4 8
        attribs( 1,-1, 1, 1, 1, 0, 0, 0, 1, 0.5 , 1/3.0f),  //1 11
        attribs(-1, 1, 1, 0, 1, 1, 0, 0, 1, 0.25, 0),       //5 9
        attribs( 1, 1, 1, 1, 1, 1, 0, 0, 1, 0.5 , 0),       //2 10
    };
    
    std::vector<Triangle> index_buffer = {
        Triangle(1 ,0 ,3),
        Triangle(0 ,2 ,3),
        Triangle(5 ,4 ,7),
        Triangle(4 ,6 ,7),
        Triangle(9 ,8 ,11),
        Triangle(8 ,10,11),
        Triangle(13,12,15),
        Triangle(12,14,15),
        Triangle(17,16,19),
        Triangle(16,18,19),
        Triangle(21,20,23),
        Triangle(20,22,23)
    };
    
    // for (auto& i: vertex_buffer) {
    //     i.pos.x += 10;
    // }
    Buffer<float>* depth_buffer = new Buffer<float>(DIMW, DIMW);
    InterpolationShader<attribs, MRT_target> shader;
    shader.tex = new Texture2D("data/texture.png");
    Mesh<attribs> msh = {&vertex_buffer, &index_buffer};

    MRT_target render_buffer;
    render_buffer.color = Buffer<RGB8>(DIMW, DIMW);
    render_buffer.normal = Buffer<vec3>(DIMW, DIMW);
    render_buffer.uv = Buffer<vec2>(DIMW, DIMW);

    Object* obj = new Object((Mesh<uint8_t>*)&msh);
    Object* obj2 = new Object((Mesh<uint8_t>*)&msh);
    obj2->set_scale(vec3(0.2, 0.2, 0.2));

    Camera* cam = new Camera(M_PI*0.5,1,1, vec3(3.0f,3.0,2.0f));
    cam->look_at(vec3(0.0f), vec3(0,0,1));
    
    RenderPipeline* render_pipeline = new RenderPipeline(DIMW, DIMW);
    render_pipeline->active_camera = cam;
    render_pipeline->render_target = &render_buffer;
    render_pipeline->depth_buffer = new Buffer<float>(DIMW, DIMW);
    render_pipeline->shader = reinterpret_cast<Shader<void, void>*>(&shader);

    Image img = Image(DIMW, DIMW);

    vector<Light> lights;

    for (int i=0; i<2; i++) {
        Light light;
        light.position = mat3(glm::rotate(rand()/20.0f,vec3(0,0,1)))*vec3(3,0,0);
        light.translate(vec3(0,0,(float(rand())/RAND_MAX)*3+0.5));
        light.compute_transform();
        // cout << light.position.x << " " << light.position.y << " " << light.position.z << " " << endl;
        light.color = vec3(1);//light.position;
        light.energy = 4;
        // cout << light.position.x << " " << light.position.y << " " << light.position.z << endl;
        lights.push_back(light);
    }

    // Texture2D* background = new Texture2D("data/marc.png");

    // lights[0].color = vec3(1.0, 0.0, 0.0);
    // lights[1].color = vec3(0.0, 1.0, 0.0);
    // lights[2].color = vec3(0.0, 0.0, 1.0);

    // lights[0].energy = 2;
    // lights[1].energy = 2;
    // lights[2].energy = 2;

    //point_light.position = vec3(-2.0, 2.0, -3.0);

    // srand(time(NULL));

    setKeyRepeatMode(true);

    shader.lights = &lights;

    color(0,255,0); // green fps counter

    clock_t start_time = 0, delta_time, shift = 0;
    int count = 0, fps = 0;
    while (!quit){
        count++;
        if (start_time-shift >= 1000000){
            shift = start_time;
            fps = count;
            count = 0;
        }
        
        delta_time = (clock() - start_time);
        start_time = clock();
        // obj->rotate(start_time/2000000.0f, 2);
        // obj->set_translation(0, sin(start_time/1000.0f)/2, 0);
        // obj->compute_transform();
        cam->update((float)delta_time/1000000.0);
        shader.view_pos = cam->position;
        // cout << cam->position.x << " " << cam->position.y << " " << cam->position.z << endl;
        obj2->set_translation((mat3(glm::rotate(start_time/2000000.0f,vec3(0,0,1)))*vec3(2.0, 0.0, 0.0)));
        // obj2->scale_matrix = glm::scale(vec3(sin(start_time/400000.0f)*0.1+0.3));
        //
        obj2->compute_transform();
        cam->compute_camera_matrix();
        winClear();
        render_pipeline->depth_buffer->fill(1000.0f);
        render_buffer.color.fill(RGB8(70,70,70));

        shader.MVP = cam->camera_matrix * obj->transform_matrix;
        shader.rotation_matrix = mat3(obj->orientation_matrix);
        shader.model_matrix = obj->transform_matrix;

        // for (int i=0; i<lights.size(); i++)
            // lights[i].position = vec3(cos(start_time/2000000.0f+i), sin(start_time/5000000.0f+i), sin(start_time/2000000.0f+i))*4.0f;

        //draw_image(background, &render_buffer.color);
        render_pipeline->draw_object(obj);

        shader.MVP = cam->camera_matrix * obj2->transform_matrix;
        shader.rotation_matrix = mat3(obj2->orientation_matrix);
        render_pipeline->draw_object(obj2);
        
        draw_buffer(&render_buffer.color, img);
        print(10, DIMW-30, ("FPS : " + to_string(fps)).c_str());
        quit = winDisplay();
    }
    
    winQuit();
}