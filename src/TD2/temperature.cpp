#include "Grapic.h"
#include "_complex.h"
#include "_color.h"

using namespace std;
using namespace grapic;

const int DIMW = 500;
const int SEG  = 10;
//Ta*(1-t)t+Tb*t 

Complex complex_interp(Complex a, Complex b, float t) {
    return (1-t)*a + t*b;
}

void temperature_draw() {
    Complex a   = {20, 50};
    Complex b   = {400, 300};
    int a_temp      = -10;
    int b_temp      = 35;

    for (int i=0; i<SEG; i++) {
        float t         = (float)i/(SEG-1);
        Complex start   = complex_interp(a,b,(float)(i)/SEG);
        Complex end     = complex_interp(a,b,(float)(i+1)/SEG);
        float temp      = a_temp*(1-t) + b_temp*t;
        Color col       = color_lerp({0,0,255}, {255,0,0}, (temp+30)/70);
        color(col.r, col.g, col.b);
        line(start.re, start.im, end.re, end.im);
    }
}

int main(int, char**) {
    winInit("Temperature interpolation", DIMW, DIMW);
    temperature_draw();
    pressSpace();
    winQuit();
    return 0;
}