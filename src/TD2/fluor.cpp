// 1500*(5/13) + 700*(8/13) = 1007,692307....
#include "Grapic.h"
#include <iostream>
#include "_complex.h"

using namespace std;
using namespace grapic;

const int DIMW = 500;
const int TEMPS[10] = {11,14,16,17,19,16,16,13,12,11};

struct Heur {
    int heure, minute;
};

float teneur_en_fluor(float temperature) {
    if (temperature <= 12)
        return 1500;
    if (temperature >= 25)
        return 700;
    float coeff = float(700-1500)/float(25-12);
    float offset = 1500-coeff*12.0f;
    return coeff*temperature + offset;
}

float temperature(const int tab[10], Heur t) {
    int mins = t.heure*60 + t.minute;

    if (t.heure == 18 && t.minute == 0)
        return tab[18];

    if (mins >= 9*60 && mins < 18*60) {
        float factor = (t.minute/60);
        return tab[t.heure - 9]*(1-factor) + tab[t.heure - 8]*factor;
    } else 
        cout << "Heure incorrecte" << endl;
    return NULL;
}

void affiche_courbe() {

    color(255,0,0);

    for (int i=0; i<9; i++) {
        int start_x = DIMW/9*i;
        int start_y = (TEMPS[i]-11)*(DIMW/8);
        int end_x   = DIMW/9*(i+1);
        int end_y   = (TEMPS[i+1]-11)*(DIMW/8);
        line(start_x, start_y, end_x, end_y);
    }
}


int main(int, char**) {
    winInit("courbe fluor", DIMW, DIMW);
    backgroundColor(0,0,0);
    affiche_courbe();
    pressSpace();
    winQuit();
    cout << teneur_en_fluor(17) << endl;
}