#include "Grapic.h"
#include "point.h"

using namespace std;
using namespace grapic;

const int DIMW  = 500;
const int MAX   = 64;

struct Polygon {
    int n;
    Point p[MAX];
};

void polygon_add(Polygon& p, float px, float py) {
    if (p.n < MAX) {
        p.p[p.n] = make_point(px, py);
        p.n++;
    }
}

Point Point_interp(Point a, Point b, float t) {
    return (1-t)*a + t*b;
}


Point center_of_mass(Polygon p) {
    Point res = make_point(0,0);
    for (int i=0; i<p.n; i++)
        res = res+p.p[i];
    return (1.0/p.n)*res;
}

void polygon_draw(Polygon p) {
    
    color(255,255,255);
    for (int i=0; i<p.n; i++) {
        Point end = p.p[(i+1) % p.n];
        Point start = p.p[i];
        line(start.x, start.y, end.x, end.y);
    }
    if (p.n > 0) {
        Point com = center_of_mass(p);
        circleFill(com.x,com.y, 5);
    }
}

Polygon poly_interp(Polygon a, Polygon b, float t) {
    Polygon res;
    res.n = 0;
    if (a.n == b.n) {
        for (int i=0; i<a.n; i++) {
            Point comp = Point_interp(a.p[i], b.p[i], t);
            polygon_add(res, comp.x, comp.y);
        }
    }
    return res;
}


void polygon_scale(Polygon& p, float cx, float cy, float lambda) {
    Point center = make_point(cx, cy);
    for (int i=0; i<p.n; i++) {
        p.p[i] = lambda*(p.p[i]-center)+center;
    }   
}

void polygon_rotate(Polygon& p, float cx, float cy, float theta) {
    Point center = make_point(cx, cy);
    Point angle  = make_point_exp(1, theta);
    for (int i=0; i<p.n; i++) {
        p.p[i] = (p.p[i]-center)*angle+center;
    }   
}

void polygon_symmetry(Polygon& p, float cx, float cy, float theta) {
    Point center = center_of_mass(p);
    for (int i=0; i<p.n; i++) {
        p.p[i] = -1*(p.p[i]-center)+center;
    }   
}


void translate(Polygon& p, float dx, float dy) {
    Point t = make_point(dx,dy);
    for (int i=0; i<p.n; i++)
        p.p[i] = p.p[i] + t;
}

void update(Polygon& p) {
    if (isKeyPressed(SDLK_SPACE)) {
        int x,y;
        mousePos(x,y);
        polygon_add(p, x,y);
    }
    Point com = center_of_mass(p);
    if (isKeyPressed(SDLK_LEFT))
        polygon_rotate(p, com.x, com.y, 20);
    if (isKeyPressed(SDLK_RIGHT))
        polygon_rotate(p, com.x, com.y, -20);
    if (isKeyPressed(SDLK_UP))
        polygon_scale(p, com.x, com.y, 1.3);
    if (isKeyPressed(SDLK_DOWN))
        polygon_scale(p, com.x, com.y, 0.7);
}

int main(int, char**) {
    winInit("polygone", DIMW, DIMW);
    backgroundColor(0,0,0);
    setKeyRepeatMode(false);
    Polygon p;
    p.n = 0;
    // polygon_add(p, 50,50);
    // polygon_add(p, 250,450);
    // polygon_add(p, 450,50);
    bool quit = false;
    while (!quit)
    {
        winClear();
        update(p);
        polygon_draw(p);
        quit = winDisplay();
    }
    // polygon_draw(p);
    pressSpace();
    winQuit();
}