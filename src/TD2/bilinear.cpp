#include "Grapic.h"
#include <iostream>
#include "_color.h"

using namespace std;
using namespace grapic;

const int DIMW = 500;

void draw_quad_bilineare() {

    Color p0 = {255,255,0};
    Color p1 = {0,0,255};
    Color p2 = {255,0,0};
    Color p3 = {0,255,0};

    for (int i=0; i<DIMW; i++) {
        for (int j=0; j<DIMW; j++) {
            float x_fact = (float)i/(DIMW-1);
            Color E = color_lerp(p0, p1, x_fact);
            Color F = color_lerp(p2, p3, x_fact);
            Color G = color_lerp(F,E, (float)j/(DIMW-1));
            put_pixel(i,j,G.r,G.g,G.b);
        }
    }

}

int main(int, char**) {
    winInit("test", 500, 500);
    draw_quad_bilineare();
    pressSpace();
    winQuit();

}